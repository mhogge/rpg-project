﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour
    {
        [SerializeField] float waypointGizmoRadius = 0.1f;
        [SerializeField] Color waypointGizmoColor = Color.gray;
        private void OnDrawGizmos() {
            Gizmos.color = waypointGizmoColor;
            for (int i = 0; i < GetNbWaypoints(); i++)
            {
                Gizmos.DrawSphere(GetWaypoint(i), waypointGizmoRadius);
                Gizmos.DrawLine(GetWaypoint(i), GetWaypoint(GetNextIndex(i)));
            }
        }

        public Vector3 GetWaypoint(int i)
        {
            return transform.GetChild(i).position;
        }

        public int GetNextIndex(int i) {
            if (i + 1 == GetNbWaypoints()) {
                return 0;
            }
            return i + 1;
        }

        private int GetNbWaypoints() {
            return transform.childCount;
        }
    }
}


