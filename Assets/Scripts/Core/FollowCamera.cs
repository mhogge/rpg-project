﻿using UnityEngine;

namespace RPG.Core {
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] Transform target; // Transform -> get the details of position, ... see "transform" in inspector.
        [SerializeField] float minDistance = 1f;
        [SerializeField] float maxDistance = 10f;
        [SerializeField] float zoomMultiplier = 2f;

        public float speedH = 2.0f;
        public float speedV = 2.0f;

        private float yaw = 0.0f;
        private float pitch = 0.0f;
        private Camera mainCamera;

        void Start()
        {
            mainCamera = (Camera) GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        }

        void Update ()
        {
            RotateAction();
            ZoomAction();
        }

        private void RotateAction()
        {
            if (Input.GetKey(KeyCode.LeftAlt) && Input.GetMouseButton(0))
            {
                yaw += speedH * Input.GetAxis("Mouse X");
                pitch -= speedV * Input.GetAxis("Mouse Y");

                transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            }
        }

        void ZoomAction() {
            // Inspired by https://answers.unity.com/questions/218347/how-do-i-make-the-camera-zoom-in-and-out-with-the.html
            float ScrollWheelChange = Input.GetAxis("Mouse ScrollWheel");           //This little peece of code is written by JelleWho https://github.com/jellewie
            if (ScrollWheelChange != 0) {                                            //If the scrollwheel has changed
                float R = ScrollWheelChange * zoomMultiplier;                                   //The radius from current camera
                float PosX = mainCamera.transform.eulerAngles.x + 90;              //Get up and down
                float PosY = -1 * (mainCamera.transform.eulerAngles.y - 90);       //Get left to right
                PosX = PosX / 180 * Mathf.PI;                                       //Convert from degrees to radians
                PosY = PosY / 180 * Mathf.PI;                                       //^
                float X = R * Mathf.Sin(PosX) * Mathf.Cos(PosY);                    //Calculate new coords
                float Z = R * Mathf.Sin(PosX) * Mathf.Sin(PosY);                    //^
                float Y = R * Mathf.Cos(PosX);                                      //^
                float CamX = mainCamera.transform.position.x;                      //Get current camera postition for the offset
                float CamY = mainCamera.transform.position.y;                      //^
                float CamZ = mainCamera.transform.position.z;                      //^
                Vector3 nextCameraPosition = new Vector3(CamX + X, CamY + Y, CamZ + Z);
                float distanceFromTarget = DistanceFromTarget(nextCameraPosition);
                if (distanceFromTarget >= minDistance && distanceFromTarget <= maxDistance) {
                    mainCamera.transform.position = new Vector3(CamX + X, CamY + Y, CamZ + Z); //Move the main camera
                }
            }
        }

        void LateUpdate() {
            // transform.position --> The position of whatever gameobject is attached to the script.
            transform.position = target.position;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.GetChild(0).transform.position);
        }

        float CameraDistanceFromTarget() {
            return DistanceFromTarget(mainCamera.transform.position);
        }

        private float DistanceFromTarget(Vector3 vector) {
            return Vector3.Distance(vector, target.position);
        }

    }
}

